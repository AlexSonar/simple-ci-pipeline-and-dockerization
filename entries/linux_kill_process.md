
# kill processes by name.

<pre>Syntax
      killall [_option(s)_] [--] _name_ ...

      killall -l

      killall -V,--version 

Options

   -e
   --exact
          Require an exact match for very long names.
          If a command name is longer than 15 characters, the full name might be
          unavailable (i.e. it is swapped out). In this case, killall will kill everything
          that matches within the first 15 characters. With -e, such entries are skipped. 
          killall prints a message for each skipped entry if -v is specified in addition to -e, 

   -g
   --process-group
          Kill the process group to which the process belongs.
          The kill signal is only sent once per group, even if multiple processes belonging
          to the same process group were found.

   -I     Do case insensitive process name match. 
   --ignore-case

   -i     Interactively ask for confirmation before killing. 
   --interactive

   -l     List all known signal names.
   --list

   _name_   The command/process to be killed

   -q     Do not complain if no processes were killed.
   --quiet

   -r     Interpret process name pattern as an extended regular expression.
   --regexp

   -_s_ _signal_
   --signal _signal_
          Send _signal_ instead of the default SIGTERM.  e.g. -9 = SIGKILL

   -u _user_
   --user _user_
          Kill only processes the specified user owns. Command names are optional.

   -v     Report if the signal was successfully sent.
   --verbose

   -V     Display version information.
   --version

   -w
   --wait
          Wait for all killed processes to die. killall checks once per second if any
          of the killed processes still exist and only returns if none are left.
          Note that killall can wait forever if the signal was ignored, had no effect, or
          if the process stays in zombie state. 

   -Z _pattern_
   --context _pattern_ 
          Specify security context: kill only processes having security context that match
          with given expended regular expression pattern. Must precede other arguments on
          the command line. Command names are optional. (SELinux Only)

   --     Each parameter after a '--' parameter is always interpreted as a non-option parameter.</pre>

killall sends a signal to all processes running any of the specified commands. If no signal name is specified, SIGTERM is sent.

Signals can be specified either by name (e.g. -HUP) or by number (e.g. -1) or by option -s.

If the command name is not regular expression (option -r) and contains a slash (/), processes executing that particular file will be selected for killing, independent of their name.

killall returns a zero return code if at least one process has been killed for each listed command, or no commands were listed and at least one process matched the -u and -Z search criteria. killall returns non-zero otherwise.

A killall process never kills itself (but can kill other killall processes).

> <table><caption>Common Kill Signals</caption>
> 
> <tbody>
> 
> <tr>
> 
> <th scope="col">Signal name</th>
> 
> <th scope="col">Signal value</th>
> 
> <th scope="col">Effect</th>
> 
> </tr>
> 
> <tr>
> 
> <td>SIGHUP</td>
> 
> <td>1</td>
> 
> <td>Hangup</td>
> 
> </tr>
> 
> <tr>
> 
> <td>SIGINT</td>
> 
> <td>2</td>
> 
> <td>Interrupt from keyboard</td>
> 
> </tr>
> 
> <tr>
> 
> <td>SIGQUIT</td>
> 
> <td>3</td>
> 
> <td>Quit</td>
> 
> </tr>
> 
> <tr>
> 
> <td>SIGABRT</td>
> 
> <td>6</td>
> 
> <td>Cancel</td>
> 
> </tr>
> 
> <tr>
> 
> <td>SIGKILL</td>
> 
> <td>9</td>
> 
> <td>Kill signal</td>
> 
> </tr>
> 
> <tr>
> 
> <td>SIGTERM</td>
> 
> <td>15</td>
> 
> <td>Termination signal - allow an orderly shutdown</td>
> 
> </tr>
> 
> <tr>
> 
> <td>SIGSTOP</td>
> 
> <td>17,19,23</td>
> 
> <td>Stop the process</td>
> 
> </tr>
> 
> </tbody>
> 
> </table>

Files<span class="code">/proc</span>  
location of the proc file system

## Known Bugs

> Typing <span class="code">killall _name_</span> might not have the desired effect on non-Linux systems, especially when done by a privileged user. e.g. on Solaris it will kill all active processes.
> 
> Killing by file only works for executables that are kept open during execution, i.e. impure executables can't be killed this way.
> 
> killall -w doesn't detect if a process disappears and is replaced by a new process with the same PID between scans.
> 
> If processes change their name, killall might not be able to match them correctly.

**Examples**

Kill firefox:

$ killall -9 mozilla-bin

“If future generations are to remember us with gratitude rather than contempt, we must leave them more than the miracles of technology. We must leave them a glimpse of the world as it was in the beginning, not just after we got through with it” ~ President Lyndon B. Johnson


* * *

