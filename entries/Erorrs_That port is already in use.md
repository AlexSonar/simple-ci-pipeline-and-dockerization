

netstat -ntlp

```
root@server_django_wiki_app:/# netstat -ntlp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:8000            0.0.0.0:*               LISTEN      42/python
root@server_django_wiki_app:/# kill -9 42
root@server_django_wiki_app:/# ./startd.sh

```

So now just close the port in which Django/python running already by killing the process associated with it.

kill -9 PID

in my case

kill -9 6599

Now run your Django app.





The more simple solution just type sudo fuser -k 8000/tcp. This should kill all the processes associated with port 8000.

EDIT:

For osx users you can use sudo lsof -t -i tcp:8000 | xargs kill -9

```
ps aux | grep -i manage
```

after that you will see all process 

```
ubuntu@ip-10-154-22-113:~/django-apps/projectname$ ps aux | grep -i manage
ubuntu    3439  0.0  2.3  40228 14064 pts/0    T    06:47   0:00 python manage.py runserver project name
ubuntu    3440  1.4  9.7 200996 59324 pts/0    Tl   06:47   2:52 /usr/bin/python manage.py runserver project name
ubuntu    4581  0.0  0.1   7988   892 pts/0    S+   10:02   0:00 grep --color=auto -i manage
```

```
kill -9 process id


e.d kill -9 3440
```

`enter code here`after that :

python manage.py runserver project name