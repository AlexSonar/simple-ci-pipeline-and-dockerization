import re
from . import util
from . import markdown2


# ?????
def sub_title(title):
    title = re.sub('[^A-Za-z-1-9 -_]+', '', title)
    return title


# This method return the boolean
# as checks whether such a file already exists
def check_exists_pages(page_title):
    # page_title = sub_title(page_title)
    for i in range(len(util.list_entries())):
        le = util.list_entries()[i]        
        if le == page_title:
            return True
    return False


# This method return the value
# as checks whether such a file already exists
def matching(search_request):
    matching_title = [s for s in util.list_entries() if search_request in s]    
    return matching_title


def converted_entry_content(page_title):
    return markdown2.Markdown().convert(util.get_entry(page_title))


def content(page_title, entries, exist):
    context = {
        "page_title": page_title,
        "page_content": converted_entry_content(page_title),
        "exist": exist,
        "entries": entries
    }
    return context


def if_matching(search_request):
    a = False
    if any(search_request in s for s in util.list_entries()):
        a = True
    return a


def title_replacer(title):
    for i in ['~', '`', '\\', '|', '/','+','@','#','!','$','%','^', '*','(',')','{','}','[',']', '<', '>']:
        title = title.replace(i, '')
    return title
