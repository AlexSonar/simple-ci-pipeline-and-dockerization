from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template.defaultfilters import random
from . import util, controller, controller_search, controller_edit


# index page
def index(request):
    return render(request, "encyclopedia/index.html", {
        "entries": util.list_entries()
    })


def wiki_page(request, page_title):
    # page_title = page_title.capitalize()
    if not controller.check_exists_pages(page_title):
        return HttpResponseRedirect(reverse("encyclopedia:404"))
    else:
        return render(request, "encyclopedia/page.html",
                      controller.content(
                          page_title,
                          util.list_entries(),
                          controller.check_exists_pages(page_title)))


def random_page(request):
    return render(request,
                  "encyclopedia/page.html",
                  controller.content(random(util.list_entries()),
                                     util.list_entries(), True))


# Register sensitive search
def search(request):
    return controller_search.searcher(request)


def create(request, page_title):
    page_title = " "
    action = False
    return controller_edit.editor(request, page_title, action)


def edit(request, page_title):
    action = True
    return controller_edit.editor(request, page_title, action)


def page404(request):
    return render(request, "encyclopedia/page404.html", {
        "entries": util.list_entries(),
        "count": len(util.list_entries())
    })
