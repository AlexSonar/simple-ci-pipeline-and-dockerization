from django.shortcuts import render, redirect
from django.contrib import messages
from . import util
from . import controller

# def editor(request, page_title, action):
from . import views


def editor(request, page_title, action):
    # on edit exist page
    global content1
    if action:
        content1 = util.get_entry(page_title)
        if request.method == "POST":
            form_title = request.POST.get("title_name")
            form_content = request.POST.get("content")
            util.save_entry(form_title, form_content)
            return views.wiki_page(request, form_title)
        else:
            context_page(request, url_path, action, page_title, content1)
            # on create new page
    elif not action:
        content1 = ""
        if request.method == "POST":
            form_title = request.POST.get("title_name")
            # This method return boolean
            # as checks whether such a file already exists            
            if not controller.check_exists_pages(form_title):
                form_title = controller.title_replacer(form_title)
                form_title = controller.sub_title(form_title)
                form_content = request.POST.get("content")
                util.save_entry(form_title, form_content)
                return views.wiki_page(request, form_title)
            else:
                form_content = request.POST.get("content")
                messages.warning(request,
                                 "<b>Sorry I couldn't save it</b>, because a file with this name already exists! <br> Please! Change the name, or open an existing one for editing.")
                return context_page(request, url_path, False, form_title, form_content)
    # print(f"return edit, action={action}, page_title={page_title}, content1={content1}.")    
    return context_page(request, url_path, action, page_title, content1)


def context_page(request, url, action, page_title, content):
    return render(request, url_path, {
        "action": action,
        "page_title": page_title,
        "pure_content": content
    })


url_path = "encyclopedia/edit.html"
