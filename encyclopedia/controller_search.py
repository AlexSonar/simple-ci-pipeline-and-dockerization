from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from . import util
from . import controller

list_entries = util.list_entries()


def searcher(request):
    rq = request.GET.get('q')
    page_title = rq.capitalize()    
    # This method checks whether such a file already exists
    if controller.check_exists_pages(page_title):
        return render(request, "encyclopedia/page.html", controller.content(page_title, list_entries, True))
    elif controller.if_matching(page_title):
        matching_list = controller.matching(page_title)        
        return render(request, "encyclopedia/index.html", {
            "entries": matching_list
        })
    else:
        return HttpResponseRedirect(reverse("encyclopedia:404"))

