# encyclopedia/urls.py

from django.urls import path
from . import views


app_name = "encyclopedia"
urlpatterns = [
    path("", views.index, name="index"),
    path("create/<str:page_title>", views.create, name="create"),
    path("edit/<str:page_title>", views.edit, name="edit"),
    path("404", views.page404, name="404"),
    path("search", views.search, name="search"),
    path("random", views.random_page, name="random"),
    path("<str:page_title>", views.wiki_page, name="wiki_page")    
]
